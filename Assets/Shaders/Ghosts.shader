//////////////////////////////////////////////////////////////
/// Shadero Sprite: Sprite Shader Editor - by VETASOFT 2018 //
/// Shader generate with Shadero 1.9.4                      //
/// http://u3d.as/V7t #AssetStore                           //
/// http://www.shadero.com #Docs                            //
//////////////////////////////////////////////////////////////

Shader "Shadero Customs/Ghosts"
{
Properties
{
[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
LiquidUV_WaveX_1("LiquidUV_WaveX_1", Range(0, 2)) = 1.375
LiquidUV_WaveY_1("LiquidUV_WaveY_1", Range(0, 2)) = 1.196
LiquidUV_DistanceX_1("LiquidUV_DistanceX_1", Range(0, 1)) = 0.616
LiquidUV_DistanceY_1("LiquidUV_DistanceY_1", Range(0, 1)) = 0.259
LiquidUV_Speed_1("LiquidUV_Speed_1", Range(-2, 2)) = 0.1
_NewTex_3("NewTex_3(RGB)", 2D) = "white" { }
AnimatedRotationUV_AnimatedRotationUV_Rotation_1("AnimatedRotationUV_AnimatedRotationUV_Rotation_1", Range(-360, 360)) = 0
AnimatedRotationUV_AnimatedRotationUV_PosX_1("AnimatedRotationUV_AnimatedRotationUV_PosX_1", Range(-1, 2)) = 0.35
AnimatedRotationUV_AnimatedRotationUV_PosY_1("AnimatedRotationUV_AnimatedRotationUV_PosY_1", Range(-1, 2)) = 0.586
AnimatedRotationUV_AnimatedRotationUV_Intensity_1("AnimatedRotationUV_AnimatedRotationUV_Intensity_1", Range(0, 4)) = 0.1
AnimatedRotationUV_AnimatedRotationUV_Speed_1("AnimatedRotationUV_AnimatedRotationUV_Speed_1", Range(-10, 10)) = 1.2
_NewTex_4("NewTex_4(RGB)", 2D) = "white" { }
_Mask2uv_Fade_241("_Mask2uv_Fade_241", Range(0, 1)) = 1
AnimatedRotationUV_AnimatedRotationUV_Rotation_2("AnimatedRotationUV_AnimatedRotationUV_Rotation_2", Range(-360, 360)) = 2.38
AnimatedRotationUV_AnimatedRotationUV_PosX_2("AnimatedRotationUV_AnimatedRotationUV_PosX_2", Range(-1, 2)) = 0.257
AnimatedRotationUV_AnimatedRotationUV_PosY_2("AnimatedRotationUV_AnimatedRotationUV_PosY_2", Range(-1, 2)) = 0.578
AnimatedRotationUV_AnimatedRotationUV_Intensity_2("AnimatedRotationUV_AnimatedRotationUV_Intensity_2", Range(0, 4)) = 0.06
AnimatedRotationUV_AnimatedRotationUV_Speed_2("AnimatedRotationUV_AnimatedRotationUV_Speed_2", Range(-10, 10)) = 2.4
_NewTex_5("NewTex_5(RGB)", 2D) = "white" { }
_Mask2uv_Fade_242("_Mask2uv_Fade_242", Range(0, 1)) = 0.791
_NewTex_6("NewTex_6(RGB)", 2D) = "white" { }
_Mask2uv_Fade_243("_Mask2uv_Fade_243", Range(0, 1)) = 1
_NewTex_7("NewTex_7(RGB)", 2D) = "white" { }
_OperationBlend_Fade_1("_OperationBlend_Fade_1", Range(0, 1)) = 1
AnimatedShakeUV_1_OffsetX_1("AnimatedShakeUV_1_OffsetX_1", Range(0, 0.05)) = 0.02
AnimatedShakeUV_1_OffsetY_1("AnimatedShakeUV_1_OffsetY_1", Range(0, 0.05)) = 0.022
AnimatedShakeUV_1_IntenseX_1("AnimatedShakeUV_1_IntenseX_1", Range(-3, 3)) = 0.332
AnimatedShakeUV_1_IntenseY_1("AnimatedShakeUV_1_IntenseY_1", Range(-3, 3)) = 0.46
AnimatedShakeUV_1_Speed_1("AnimatedShakeUV_1_Speed_1", Range(-1, 1)) = 0.01
_NewTex_1("NewTex_1(RGB)", 2D) = "white" { }
_NewTex_2("NewTex_2(RGB)", 2D) = "white" { }
_MaskAlpha_Fade_1("_MaskAlpha_Fade_1", Range(0, 1)) = 0
_OperationBlend_Fade_2("_OperationBlend_Fade_2", Range(0, 1)) = 1
_SpriteFade("SpriteFade", Range(0, 1)) = 1.0

// required for UI.Mask
[HideInInspector]_StencilComp("Stencil Comparison", Float) = 8
[HideInInspector]_Stencil("Stencil ID", Float) = 0
[HideInInspector]_StencilOp("Stencil Operation", Float) = 0
[HideInInspector]_StencilWriteMask("Stencil Write Mask", Float) = 255
[HideInInspector]_StencilReadMask("Stencil Read Mask", Float) = 255
[HideInInspector]_ColorMask("Color Mask", Float) = 15

}

SubShader
{

Tags {"Queue" = "Transparent" "IgnoreProjector" = "true" "RenderType" = "Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
ZWrite Off Blend SrcAlpha OneMinusSrcAlpha Cull Off 

// required for UI.Mask
Stencil
{
Ref [_Stencil]
Comp [_StencilComp]
Pass [_StencilOp]
ReadMask [_StencilReadMask]
WriteMask [_StencilWriteMask]
}

Pass
{

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#include "UnityCG.cginc"

struct appdata_t{
float4 vertex   : POSITION;
float4 color    : COLOR;
float2 texcoord : TEXCOORD0;
};

struct v2f
{
float2 texcoord  : TEXCOORD0;
float4 vertex   : SV_POSITION;
float4 color    : COLOR;
};

sampler2D _MainTex;
float _SpriteFade;
float LiquidUV_WaveX_1;
float LiquidUV_WaveY_1;
float LiquidUV_DistanceX_1;
float LiquidUV_DistanceY_1;
float LiquidUV_Speed_1;
sampler2D _NewTex_3;
float AnimatedRotationUV_AnimatedRotationUV_Rotation_1;
float AnimatedRotationUV_AnimatedRotationUV_PosX_1;
float AnimatedRotationUV_AnimatedRotationUV_PosY_1;
float AnimatedRotationUV_AnimatedRotationUV_Intensity_1;
float AnimatedRotationUV_AnimatedRotationUV_Speed_1;
sampler2D _NewTex_4;
float _Mask2uv_Fade_241;
float AnimatedRotationUV_AnimatedRotationUV_Rotation_2;
float AnimatedRotationUV_AnimatedRotationUV_PosX_2;
float AnimatedRotationUV_AnimatedRotationUV_PosY_2;
float AnimatedRotationUV_AnimatedRotationUV_Intensity_2;
float AnimatedRotationUV_AnimatedRotationUV_Speed_2;
sampler2D _NewTex_5;
float _Mask2uv_Fade_242;
sampler2D _NewTex_6;
float _Mask2uv_Fade_243;
sampler2D _NewTex_7;
float _OperationBlend_Fade_1;
float AnimatedShakeUV_1_OffsetX_1;
float AnimatedShakeUV_1_OffsetY_1;
float AnimatedShakeUV_1_IntenseX_1;
float AnimatedShakeUV_1_IntenseY_1;
float AnimatedShakeUV_1_Speed_1;
sampler2D _NewTex_1;
sampler2D _NewTex_2;
float _MaskAlpha_Fade_1;
float _OperationBlend_Fade_2;

v2f vert(appdata_t IN)
{
v2f OUT;
OUT.vertex = UnityObjectToClipPos(IN.vertex);
OUT.texcoord = IN.texcoord;
OUT.color = IN.color;
return OUT;
}


float4 OperationBlend(float4 origin, float4 overlay, float blend)
{
float4 o = origin; 
o.a = overlay.a + origin.a * (1 - overlay.a);
o.rgb = (overlay.rgb * overlay.a + origin.rgb * origin.a * (1 - overlay.a)) * (o.a+0.0000001);
o.a = saturate(o.a);
o = lerp(origin, o, blend);
return o;
}
float2 AnimatedRotationUV(float2 uv, float rot, float posx, float posy, float radius, float speed)
{
uv = uv - float2(posx, posy);
float angle = rot * 0.01744444;
angle += sin(_Time * speed * 20) * radius;
float sinX = sin(angle);
float cosX = cos(angle);
float2x2 rotationMatrix = float2x2(cosX, -sinX, sinX, cosX);
uv = mul(uv, rotationMatrix) + float2(posx, posy);
return uv;
}
float2 LiquidUV(float2 p, float WaveX, float WaveY, float DistanceX, float DistanceY, float Speed)
{ Speed *= _Time * 100;
float x = sin(p.y * 4 * WaveX + Speed);
float y = cos(p.x * 4 * WaveY + Speed);
x += sin(p.x)*0.1;
y += cos(p.y)*0.1;
x *= y;
y *= x;
x *= y + WaveY*8;
y *= x + WaveX*8;
p.x = p.x + x * DistanceX * 0.015;
p.y = p.y + y * DistanceY * 0.015;

return p;
}
float2 AnimatedShakeUV(float2 uv, float offsetx, float offsety, float zoomx, float zoomy, float speed)
{
float time = sin(_Time * speed * 5000 * zoomx);
float time2 = sin(_Time * speed * 5000 * zoomy);
uv += float2(offsetx * time, offsety * time2);
return uv;
}
float4 frag (v2f i) : COLOR
{
float2 LiquidUV_1 = LiquidUV(i.texcoord,LiquidUV_WaveX_1,LiquidUV_WaveY_1,LiquidUV_DistanceX_1,LiquidUV_DistanceY_1,LiquidUV_Speed_1);
float4 NewTex_3 = tex2D(_NewTex_3,LiquidUV_1);
float2 AnimatedRotationUV_1 = AnimatedRotationUV(i.texcoord,AnimatedRotationUV_AnimatedRotationUV_Rotation_1,AnimatedRotationUV_AnimatedRotationUV_PosX_1,AnimatedRotationUV_AnimatedRotationUV_PosY_1,AnimatedRotationUV_AnimatedRotationUV_Intensity_1,AnimatedRotationUV_AnimatedRotationUV_Speed_1);
float4 NewTex_4 = tex2D(_NewTex_4,AnimatedRotationUV_1);
float2 Mask2uv241 = lerp(i.texcoord,AnimatedRotationUV_1, lerp(NewTex_4.r, 1 - NewTex_4.r ,_Mask2uv_Fade_241));
float2 AnimatedRotationUV_2 = AnimatedRotationUV(i.texcoord,AnimatedRotationUV_AnimatedRotationUV_Rotation_2,AnimatedRotationUV_AnimatedRotationUV_PosX_2,AnimatedRotationUV_AnimatedRotationUV_PosY_2,AnimatedRotationUV_AnimatedRotationUV_Intensity_2,AnimatedRotationUV_AnimatedRotationUV_Speed_2);
float4 NewTex_5 = tex2D(_NewTex_5,AnimatedRotationUV_2);
float2 Mask2uv242 = lerp(Mask2uv241,AnimatedRotationUV_2, lerp(NewTex_5.r, 1 - NewTex_5.r ,_Mask2uv_Fade_242));
float4 NewTex_6 = tex2D(_NewTex_6, i.texcoord);
float2 Mask2uv243 = lerp(Mask2uv242,i.texcoord, lerp(NewTex_6.r, 1 - NewTex_6.r ,_Mask2uv_Fade_243));
float4 NewTex_7 = tex2D(_NewTex_7,Mask2uv243);
float4 OperationBlend_1 = OperationBlend(NewTex_3, NewTex_7, _OperationBlend_Fade_1); 
float2 AnimatedShakeUV_1 = AnimatedShakeUV(i.texcoord,AnimatedShakeUV_1_OffsetX_1,AnimatedShakeUV_1_OffsetY_1,AnimatedShakeUV_1_IntenseX_1,AnimatedShakeUV_1_IntenseY_1,AnimatedShakeUV_1_Speed_1);
float4 NewTex_1 = tex2D(_NewTex_1,AnimatedShakeUV_1);
float4 NewTex_2 = tex2D(_NewTex_2,AnimatedShakeUV_1);
float4 MaskAlpha_1=NewTex_1;
MaskAlpha_1.a = lerp(NewTex_2.a, 1 - NewTex_2.a,_MaskAlpha_Fade_1);
float4 OperationBlend_2 = OperationBlend(OperationBlend_1, MaskAlpha_1, _OperationBlend_Fade_2); 
float4 FinalResult = OperationBlend_2;
FinalResult.rgb *= i.color.rgb;
FinalResult.a = FinalResult.a * _SpriteFade * i.color.a;
return FinalResult;
}

ENDCG
}
}
Fallback "Sprites/Default"
}
